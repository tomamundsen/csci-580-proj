/*
+--------------------------+
|         CSCI 480         |
| Assignment 3 - Raytracer |
|--------------------------|
| Name: Rohan Saini        |
| Date: 2012-04-08         |
+--------------------------+
*/

#include <iostream>
#include <iomanip>
#include <windows.h>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>

#include <pic.h>
#include <GL/glu.h>
#include <GL/glut.h>

#define _USE_MATH_DEFINES
#include <math.h>

#define MAX_TRIANGLES 10000
#define MAX_SPHERES 10000
#define MAX_LIGHTS 1000

#define WIDTH 640
#define HEIGHT 480

#define MODE_DISPLAY 1
#define MODE_JPEG 2

#define fov 60.0 // Field of View

using namespace std;

class Color
{
public:
	double r;
	double g;
	double b;

	void assign(double a, double b, double c);
};

void Color::assign(double x, double y, double z)
{
	r = x;
	g = y;
	b = z;
}


class Point
{
public:
	double x;
	double y;
	double z;

	void assign(double a, double b, double c);
	void correctZero();
};

void Point::assign(double a, double b, double c)
{
	x = a;
	y = b;
	z = c;
}

void Point::correctZero()
{
	
	if ( (x < 0.00001 && x > 0) || (x > -0.00001 && x < 0) ) {
		x = 0.0;
	}
	if ( (y < 0.00001 && y > 0) || (y > -0.00001 && y < 0) ) {
		y = 0.0;
	}
	if ( (z < 0.00001 && z > 0) || (z > -0.00001 && z < 0) ) {
		z = 0.0;
	}
	
}


struct Ray
{
	Point origin;
	Point direction;
	Color color;
	bool hitsObject;
};


struct Vertex
{
	Point position;
	Point normal;
	Color color_diffuse;
	Color color_specular;
	double shininess;
};


struct Triangle
{
	Vertex v[3];
	Point normal; // the normal to the plane of the triangle.
	double d; // the d in ax + by + cz + d = 0 ie the plane of the triangle
	int projectionAxis; // the axis on which to project the triangle
	                    // 0 -> x axis;
	                    // 1 -> y axis;
	                    // 2 -> z axis;
};


struct Sphere
{
	Point position;
	double radius;
	Color color_diffuse;
	Color color_specular;
	double shininess;
};


struct Light
{
	Point position;
	Color color;
};


//---------------------------- Scene Objects ----------------------------//
Triangle triangles[MAX_TRIANGLES];
Sphere spheres[MAX_SPHERES];
Light lights[MAX_LIGHTS];
Color ambient_light;

int num_triangles = 0;
int num_spheres = 0;
int num_lights = 0;
//-----------------------------------------------------------------------//

int mode = MODE_DISPLAY;
char *filename = 0;
unsigned char buffer[HEIGHT][WIDTH][3];

double aspectRatio;

int numRays;
Ray sceneRay[WIDTH * HEIGHT * 4];
Ray superRay[WIDTH * HEIGHT];

Color motionColor[WIDTH*HEIGHT][3]; // 3 - Buffer for motion blur

int count = 0; // number of times frame is rendered.

int initialLights; // initial number of lights in the scene file. More lights are
                    // added for softShadows.
int newLights; // Added lights to the scene, for softShadows					
bool softShadows = false; // if true, softShadows are usedj -> considerably slow.
bool motionBlur = false;   // if true, motion blur is applied
bool animation = false;   // if true, first sphere and first triangle are moved on the x axis.

//-----------------------------------------------------------------------//


Point multiplyVector(Point v, double multiplier)
{
	v.x = v.x * multiplier;
	v.y = v.y * multiplier;
	v.z = v.z * multiplier;
	return v;
}


Point addVectors(Point a, Point b)
{
	Point c;
	c.x = a.x + b.x;
	c.y = a.y + b.y;
	c.z = a.z + b.z;
	return c;
}


Point subtractVectors(Point a, Point b)
{
	Point c;
	c.x = a.x - b.x;
	c.y = a.y - b.y;
	c.z = a.z - b.z;
	return c;
}


Point normalizeVector (Point p)
{
	Point pNormal;

	double sqrt_sum_squares = sqrt( pow(p.x,2) + pow(p.y,2) + pow(p.z,2) );

	if (sqrt_sum_squares == 0) {
		pNormal.x = 0;
		pNormal.y = 0;
		pNormal.z = 0;

	} else {
		pNormal.x = p.x / sqrt_sum_squares;
		pNormal.y = p.y / sqrt_sum_squares;
		pNormal.z = p.z / sqrt_sum_squares;
	}

	pNormal.correctZero();
	return pNormal;
}


double dotProduct(Point a, Point b)
{
	double dp;

	dp = a.x * b.x +
		 a.y * b.y +
		 a.z * b.z ;

	return dp;
}


Point crossProduct(Point p1, Point p2)
{
	Point p;

	double ax = p1.x;
	double ay = p1.y;
	double az = p1.z;

	double bx = p2.x;
	double by = p2.y;
	double bz = p2.z;

	p.x = (ay * bz) - (az * by);
	p.y = (az * bx) - (ax * bz);
	p.z = (ax * by) - (ay * bx);

	if (p.x == 0) {
		p.x = 0.0;
	}
	if (p.y == 0) {
		p.y = 0.0;
	}
	if (p.z == 0) {
		p.z = 0.0;
	}


	return p;
}


void printElements()
{
	cout << "Elements:\n";
	cout << "Ambient Light:" << ambient_light.r << " " << ambient_light.g << " " << ambient_light.b << "\n";
	cout << "\nTriangles:\n";
	for (int i = 0; i < num_triangles; i++) {
		cout << "\tTriangle[" << i << "]:\n";
		for (int j = 0; j < 3; j++) {
			cout << "\t\tVertex[" << j << "]:\n";
			cout << "\t\tPosition: " << triangles[i].v[j].position.x << " " << triangles[i].v[j].position.y << " " << triangles[i].v[j].position.z << "\n";
			cout << "\t\tNormal: " << triangles[i].v[j].normal.x << " " << triangles[i].v[j].normal.y << " " << triangles[i].v[j].normal.z << "\n";
			cout << "\t\tDiffuse: " << triangles[i].v[j].color_diffuse.r << " " << triangles[i].v[j].color_diffuse.g << " " << triangles[i].v[j].color_diffuse.b << "\n";
			cout << "\t\tSpecular: " << triangles[i].v[j].color_specular.r << " " << triangles[i].v[j].color_specular.g << " " << triangles[i].v[j].color_specular.b << "\n";
			cout << "\t\tShine :" << triangles[i].v[j].shininess << "\n";
		}
	}
	cout << "\nSpheres:\n";
	for (int i = 0; i < num_spheres; i++) {
		cout << "\tSphere[" << i << "]:\n";
		cout << "\t\tPosition: " << spheres[i].position.x << " " << spheres[i].position.y << " " << spheres[i].position.z << "\n";
		cout << "\t\tRadius: " << spheres[i].radius << "\n";
		cout << "\t\tDiffuse: " << spheres[i].color_diffuse.r << " " << spheres[i].color_diffuse.g  << " " << spheres[i].color_diffuse.b << endl;
		cout << "\t\tSpecular: " << spheres[i].color_specular.r << " " << spheres[i].color_specular.g << " " << spheres[i].color_specular.b << endl;
		cout << "\t\tShine: " << spheres[i].shininess << endl;
	}
	cout << "\nLights:\n";
	for (int i = 0; i < num_lights; i++) {
		cout << "\tLight[" << i << "]:\n";
		cout << "\t\tPosition: " << lights[i].position.x << " " << lights[i].position.y << " " << lights[i].position.z << endl;
		cout << "\t\tColor: " << lights[i].color.r << " " << lights[i].color.g << " " << lights[i].color.b << endl;
	}
}


void printRay(Ray* ray)
{
	cout << "(";
	cout.precision(4);
	cout.width(7);
	cout << fixed << ray->origin.x;
	cout << ", ";
	cout.precision(4);
	cout.width(7);
	cout << fixed << ray->origin.y;
	cout << ", ";
	cout.precision(4);
	cout.width(7);
	cout << fixed << ray->origin.z;
	cout << ")   -->   ";

	cout << "(";
	cout.precision(5);
	cout.width(8);
	cout << fixed << ray->direction.x;
	cout << ", ";
	cout.precision(5);
	cout.width(8);
	cout << fixed << ray->direction.y;
	cout << ", ";
	cout.precision(5);
	cout.width(8);
	cout << fixed << ray->direction.z;
	cout << ") Hits: " << ray->hitsObject << "\n";
}


void initializeRays()
{
	aspectRatio = (double)WIDTH / (double)HEIGHT;
	numRays = WIDTH * HEIGHT * 4;

	double yMax = tan(M_PI * fov / (2.0 * 180.0) );
	double xMax = aspectRatio * yMax;
	double xDelta = xMax / (double)WIDTH;
	double yDelta = yMax / (double)HEIGHT;

	sceneRay[0].origin.assign(0, 0, 0);

	sceneRay[0].direction.x = -xMax + (xDelta / 2.0);
	sceneRay[0].direction.y = yMax - (yDelta / 2.0);
	sceneRay[0].direction.z = -1.0;
	sceneRay[0].direction.correctZero();

	sceneRay[0].color.assign(1, 1, 1);

	for (int i = 1; i < numRays; i++) {

		sceneRay[i].origin.assign(0.0, 0.0, 0.0);

		if (i % (WIDTH*2) == 0) {
			sceneRay[i].direction.x = -xMax + (xDelta / 2.0);
			sceneRay[i].direction.y = sceneRay[i-1].direction.y - yDelta;
		} else {
			sceneRay[i].direction.x = sceneRay[i-1].direction.x + xDelta;
			sceneRay[i].direction.y = sceneRay[i-1].direction.y;
		}
		sceneRay[i].direction.z = -1.0;
		sceneRay[i].direction.correctZero();

		sceneRay[i].color.assign(1, 1, 1);
	}

	for (int i = 0; i < numRays; i++) {
		sceneRay[i].direction = normalizeVector(sceneRay[i].direction);
	}
}


void initializeTriangles()
{
	for (int i = 0; i < num_triangles; i++) {

		//--------- Calculate surface normal of the triangle ------------//
		Point b_a = subtractVectors(triangles[i].v[1].position, triangles[i].v[0].position);
		Point c_a = subtractVectors(triangles[i].v[2].position, triangles[i].v[0].position);

		Point normal = crossProduct(c_a, b_a);
		normal = normalizeVector(normal);

		triangles[i].normal = normal;
		
		//----------- Calculate d in (ax + by + cz + d = 0) -------------//
		triangles[i].d = -1 * (
			                    normal.x * triangles[i].v[0].position.x + 
								normal.y * triangles[i].v[0].position.y + 
								normal.z * triangles[i].v[0].position.z
							  );

		//-------------- Calculate the projectionAxis -------------------//
		double max = fabs(normal.x);
		triangles[i].projectionAxis = 0;

		if (fabs(normal.y) > max) {
			max = fabs(normal.y);
			triangles[i].projectionAxis = 1;
		}
		if (fabs(normal.z) > max) {
			triangles[i].projectionAxis = 2;
		}
	}
}



// Returns t, if t >= 0.0001
// Returns 0, if t <  0.0001
double intersectSphere(Ray* ray, Sphere* sphere)
{
	double b, c, discriminant;
	double t;

	b = 2 * (
			  ray->direction.x * (ray->origin.x - sphere->position.x) + 
			  ray->direction.y * (ray->origin.y - sphere->position.y) + 
			  ray->direction.z * (ray->origin.z - sphere->position.z)
			);

	c = pow( (ray->origin.x - sphere->position.x), 2) + 
		pow( (ray->origin.y - sphere->position.y), 2) + 
		pow( (ray->origin.z - sphere->position.z), 2) -
		pow( sphere->radius, 2);

	discriminant = (b*b) - (4*c);
	
	if (discriminant < 0) {
		return 0;
	}

	if (discriminant == 0) {
		t = (-b/2);

	} else {

		double t0 = (-b + sqrt(discriminant)) / 2.0;
		double t1 = (-b - sqrt(discriminant)) / 2.0;
		
		if (t0 <= 0.00001 && t1 <= 0.00001) {
			return 0;

		} else if (t0 >= 0.00001 && t1 < 0.00001) {
			t = t0;

		} else if (t0 < 0.00001 && t1 >= 0.00001) {
			t = t1;

		} else {
			t = (t0 < t1) ? t0 : t1;
		}
	}

	return t;
}


double areaTriangle2D(Point a, Point b, Point c)
{
	double area = 0.5 * ( (b.x - a.x) * (c.y - a.y) - (c.x - a.x) * (b.y - a.y));
	return area;
}



/*----------------- Function triangleBarycentric() ------------------------
Input:
------
a, b, c -> Triangle Coordinates
p       -> Point to dertermine inside or outside

Output:
-------
int     ->  1: p inside triangle abc
        ->  0: p outside triangle abc
		-> -1: could not determine (area of abc == 0)

alpha   |
beta    -> barycentric weights
gamma   |
-------------------------------------------------------------------------*/
int triangleBarycentric(Point a, Point b, Point c, Point p, double* alpha, double* beta, double* gamma)
{
	double abc = areaTriangle2D(a, b, c);
	double pbc = areaTriangle2D(p, b, c);
	double apc = areaTriangle2D(a, p, c);
	double abp = areaTriangle2D(a, b, p);

	if (abc != 0) {
		*alpha = pbc/abc;
		*beta  = apc/abc;
		*gamma = abp/abc;

	} else {
		return -1;
	}

	if (*alpha < 0 || *beta < 0 || *gamma < 0) {
		return 0;
	} else {
		return 1;
	}
}



/*
//------------------ Function intersectTriangle() -----------------------//
Output:
-------
double  -> t if ray intersects the triangle
        -> 0 otherwise

alpha   |
beta    -> barycentric weights
gamma   |
*/
double intersectTriangle(Ray* ray, Triangle* triangle, double* alpha, double* beta, double* gamma)
{
	//----- Calculate t = Point where ray intersects triangle plane -----//
	double n_dot_d = dotProduct(triangle->normal, ray->direction);
	
	if (n_dot_d == 0) { // Ray parallel to triangle plane
		return 0;
	}

	double n_dot_o = dotProduct(triangle->normal, ray->origin);

	double t = (-1 * (n_dot_o + triangle->d) ) / n_dot_d;

	if (t < 0.00001) {
		return 0;
	}

	Point p; // Intersection Point
	p = multiplyVector(ray->direction, t);
	p = addVectors(ray->origin, p);


	//---------------- Project the triangle in 2d plane -----------------//
	Point a, b, c; // 2D points of the projected triangle

	if (triangle->projectionAxis == 0) { // Project on Y-Z plane

		a.assign(triangle->v[0].position.z, triangle->v[0].position.y, 0);
		b.assign(triangle->v[1].position.z, triangle->v[1].position.y, 0);
		c.assign(triangle->v[2].position.z, triangle->v[2].position.y, 0);
		p.assign(p.z, p.y, 0);

	} else if (triangle->projectionAxis == 1) { // Project on X-Z plane

		a.assign(triangle->v[0].position.x, triangle->v[0].position.z, 0);
		b.assign(triangle->v[1].position.x, triangle->v[1].position.z, 0);
		c.assign(triangle->v[2].position.x, triangle->v[2].position.z, 0);
		p.assign(p.x, p.z, 0);

	} else { // Project on X-Y Plane

		a.assign(triangle->v[0].position.x, triangle->v[0].position.y, 0);
		b.assign(triangle->v[1].position.x, triangle->v[1].position.y, 0);
		c.assign(triangle->v[2].position.x, triangle->v[2].position.y, 0);
		p.assign(p.x, p.y, 0);
	}

	*alpha = 0; *beta = 0; *gamma = 0;

	int inside = triangleBarycentric(a, b, c, p, alpha, beta, gamma);

	if (inside != 1) {
		return 0;
	}

	return t;
}



/*
 l     -> unit vector to light
 n     -> unit normal to surface
 v     -> unit vector to viewer
 kd    -> diffuse coefficient
 ks    -> specular coefficient
 alpha -> shininess coefficient
 L     -> light intensity
 */
double phongLighting(Point l, Point n, Point v, double kd, double ks, double alpha, double L)
{
	double I; // Phong lighting

	// Calculate unit reflected vector r
	Point r = multiplyVector(n, 2 * dotProduct(l, n));
	r = subtractVectors(r, l);
	r = normalizeVector(r);

	double l_n = dotProduct(l, n);
	if (l_n < 0) {
		l_n = 0;
	}

	double r_v = dotProduct(r, v);
	if (r_v < 0) {
		r_v = 0;
	}

	double specular = ks * pow(r_v, alpha);
	if (specular > 1) {
		specular = 1;
	}

	double diffuse = kd * l_n;
	if (diffuse > 1) {
		diffuse = 1;
	}

	I = L * ( diffuse + specular );
	if (I > 1) {
		I = 1;
	}

	return I;
}


void rayTrace(Ray* ray)
{
	Point intersection;
	Point normal;

	Sphere* sphere;

	Triangle* triangle;
	double alpha, beta, gamma;
	double t_alpha, t_beta, t_gamma;

	int intersectingObject = -1; // 0 -> sphere, 1 -> triangle

	double t = 0;

	for (int i = 0; i < num_spheres; i++) {

		double t1 = intersectSphere(ray, &spheres[i]);

		if (t == 0 || (t1 > 0 && t1 < t)) {
			t = t1;
			intersectingObject = 0;
			sphere = &spheres[i];
		}
	}
	
	for (int i = 0; i < num_triangles; i++) {

		double t1 = intersectTriangle(ray, &triangles[i], &t_alpha, &t_beta, &t_gamma);

		if (t == 0 || (t1 > 0 && t1 < t)) {
			t = t1;
			alpha = t_alpha; beta = t_beta; gamma = t_gamma;
			intersectingObject = 1;
			triangle = &triangles[i];
		}
	}
	
	// If ray intersects somewhere
	if (t > 0) {

		ray->hitsObject = true;

		// Calculate intersection point
		Point temp = multiplyVector(ray->direction, t);
		intersection = addVectors(ray->origin, temp);

		// Calculate Normal
		if (intersectingObject == 0) { // Intersection with sphere

			normal = subtractVectors(intersection, sphere->position);
			normal = multiplyVector(normal, (1/sphere->radius));
			normal = normalizeVector(normal);

		} else { // Intersection with triangle
			
			// Interpolate normals according to Phong Shading
			Point temp1, temp2, temp3;
			temp1 = multiplyVector(triangle->v[0].normal, alpha);
			temp2 = multiplyVector(triangle->v[1].normal, beta);
			temp3 = multiplyVector(triangle->v[2].normal, gamma);

			normal = addVectors(temp1, temp2);
			normal = addVectors(normal, temp3);
			normal = normalizeVector(normal);
		}
		
		// Assign black color to the ray
		ray->color.assign(0.0, 0.0, 0.0);

		for (int i = 0; i < num_lights; i++) {

			Ray shadowRay;
			shadowRay.origin = intersection;
			shadowRay.direction = subtractVectors(lights[i].position, intersection);
			shadowRay.direction = normalizeVector(shadowRay.direction);

			bool shadowIntersects = false;

			double temp_t;
			double t_light = (lights[i].position.x - intersection.x) / shadowRay.direction.x;
			
			for (int j = 0; j < num_spheres; j++) {

				temp_t = intersectSphere(&shadowRay, &spheres[j]);

				if (temp_t > 0 && temp_t < t_light) {
					shadowIntersects = true;
					break;
				}
			}

			if (!shadowIntersects) {

				double temp_alpha, temp_beta, temp_gamma;

				for (int j = 0; j < num_triangles; j++) {
					
					temp_t = intersectTriangle(&shadowRay, &triangles[j], &temp_alpha, &temp_beta, &temp_gamma);

					if (temp_t > 0 && temp_t < t_light) {
						shadowIntersects = true;
						break;
					}
				}
			}
			

			if (!shadowIntersects) { // Calculate Phong model

				// Add ambient light, if not already added
				if (ray->color.r == 0 && ray->color.g == 0 && ray->color.b == 0) {
					ray->color.r = ambient_light.r;
					ray->color.g = ambient_light.g;
					ray->color.b = ambient_light.b;
				}

				Point viewer = multiplyVector(ray->direction, -1);

				if (intersectingObject == 0) { // Sphere

					ray->color.r += phongLighting(shadowRay.direction, normal, viewer, sphere->color_diffuse.r, sphere->color_specular.r, sphere->shininess, lights[i].color.r);
					ray->color.g += phongLighting(shadowRay.direction, normal, viewer, sphere->color_diffuse.g, sphere->color_specular.g, sphere->shininess, lights[i].color.g);
					ray->color.b += phongLighting(shadowRay.direction, normal, viewer, sphere->color_diffuse.b, sphere->color_specular.b, sphere->shininess, lights[i].color.b);

				} else { // Triangle
					
					// Interpolate color according to Phong Model
					double diffuse_r, diffuse_g, diffuse_b, specular_r, specular_g, specular_b, shininess;

					diffuse_r = alpha * triangle->v[0].color_diffuse.r +
						        beta  * triangle->v[1].color_diffuse.r +
								gamma * triangle->v[2].color_diffuse.r ;

					diffuse_g = alpha * triangle->v[0].color_diffuse.g +
								beta  * triangle->v[1].color_diffuse.g +
								gamma * triangle->v[2].color_diffuse.g ;

					diffuse_b = alpha * triangle->v[0].color_diffuse.b +
								beta  * triangle->v[1].color_diffuse.b +
								gamma * triangle->v[2].color_diffuse.b ;

					specular_r = alpha * triangle->v[0].color_specular.r +
								 beta  * triangle->v[1].color_specular.r +
								 gamma * triangle->v[2].color_specular.r ;

					specular_g = alpha * triangle->v[0].color_specular.g +
								 beta  * triangle->v[1].color_specular.g +
								 gamma * triangle->v[2].color_specular.g ;

					specular_b = alpha * triangle->v[0].color_specular.b +
								 beta  * triangle->v[1].color_specular.b +
								 gamma * triangle->v[2].color_specular.b ;

					shininess = alpha * triangle->v[0].shininess +
								beta  * triangle->v[1].shininess +
								gamma * triangle->v[2].shininess ;
					
					ray->color.r += phongLighting(shadowRay.direction, normal, viewer, diffuse_r, specular_r, shininess, lights[i].color.r);
					ray->color.g += phongLighting(shadowRay.direction, normal, viewer, diffuse_g, specular_g, shininess, lights[i].color.g);
					ray->color.b += phongLighting(shadowRay.direction, normal, viewer, diffuse_b, specular_b, shininess, lights[i].color.b);
				}

				// Clamp color to 1.0
				if (ray->color.r > 1.0) {
					ray->color.r = 1.0;
				}
				if (ray->color.g > 1.0) {
					ray->color.g = 1.0;
				}
				if (ray->color.b > 1.0) {
					ray->color.b = 1.0;
				}
			}
		}

	} else {
		ray->hitsObject = false;
	}
}


void save_jpg(int frameNumber)
{
	Pic *in = NULL;
	in = pic_alloc(WIDTH, HEIGHT, 3, NULL);

	stringstream ss;
	if (frameNumber < 10) {
		ss << "Pic\\" << filename << "_00" << frameNumber << ".jpg";
	} else if (frameNumber < 100) {
		ss << "Pic\\" << filename << "_0"  << frameNumber << ".jpg";
	} else {
		ss << "Pic\\" << filename << "_"   << frameNumber << ".jpg";
	}

	string fileName = ss.str();

	printf("Saving JPEG file: %s\n", fileName.c_str());

	memcpy(in->pix, buffer, 3 * WIDTH * HEIGHT);

	if (jpeg_write((char*) fileName.c_str(), in))
		printf("File saved Successfully\n");
	else
		printf("Error in Saving\n");

	pic_free(in);
}


void plot_pixel_display(int x,int y,unsigned char r,unsigned char g,unsigned char b)
{
	glColor3f(((double)r)/256.f,((double)g)/256.f,((double)b)/256.f);
	glVertex2i(x,y);
}


void plot_pixel_jpeg(int x,int y,unsigned char r,unsigned char g,unsigned char b)
{
	buffer[HEIGHT-y-1][x][0]=r;
	buffer[HEIGHT-y-1][x][1]=g;
	buffer[HEIGHT-y-1][x][2]=b;
}


void plot_pixel(int x,int y,unsigned char r,unsigned char g, unsigned char b)
{
	plot_pixel_display(x,y,r,g,b);
	if(mode == MODE_JPEG)
		plot_pixel_jpeg(x,y,r,g,b);
}


void draw_scene()
{
	initializeTriangles();

	for (int i = 0; i < numRays; i++) {
		sceneRay[i].color.assign(1, 1, 1);
		sceneRay[i].hitsObject = false;

		rayTrace(&sceneRay[i]);
	}
	
	for (int i = 0; i < (2 * HEIGHT) - 1; i+= 2) {
		
		for (int j = 0; j < (2*WIDTH) - 1; j+= 2) {
		
			superRay[(i/2) * WIDTH + (j/2)].color.r =   0.25 * sceneRay[ (  i    * 2 * WIDTH) + j   ].color.r +
													    0.25 * sceneRay[ (  i    * 2 * WIDTH) + j+1 ].color.r + 
													    0.25 * sceneRay[ ( (i+1) * 2 * WIDTH) + j   ].color.r + 
													    0.25 * sceneRay[ ( (i+1) * 2 * WIDTH) + j+1 ].color.r ;
			
			superRay[(i/2) * WIDTH + (j/2)].color.g =   0.25 * sceneRay[ (  i    * 2 * WIDTH) + j   ].color.g +
													    0.25 * sceneRay[ (  i    * 2 * WIDTH) + j+1 ].color.g + 
													    0.25 * sceneRay[ ( (i+1) * 2 * WIDTH) + j   ].color.g + 
													    0.25 * sceneRay[ ( (i+1) * 2 * WIDTH) + j+1 ].color.g ;
			
			superRay[(i/2) * WIDTH + (j/2)].color.b =   0.25 * sceneRay[ (  i    * 2 * WIDTH) + j   ].color.b +
													    0.25 * sceneRay[ (  i    * 2 * WIDTH) + j+1 ].color.b + 
													    0.25 * sceneRay[ ( (i+1) * 2 * WIDTH) + j   ].color.b + 
													    0.25 * sceneRay[ ( (i+1) * 2 * WIDTH) + j+1 ].color.b ;
		}
	}
}


void parse_check(char *expected,char *found)
{
	if(stricmp(expected,found)) {
		char error[100];
		printf("Expected '%s ' found '%s '\n",expected,found);
		printf("Parse error, abnormal abortion\n");
		return;
	}

}


void parse_doubles(FILE*file, char *check, double p[3])
{
	char str[100];
	fscanf(file,"%s",str);
	parse_check(check,str);
	fscanf(file,"%lf %lf %lf",&p[0],&p[1],&p[2]);
	//printf("%s %lf %lf %lf\n",check,p[0],p[1],p[2]);
}


void parse_rad(FILE*file,double *r)
{
	char str[100];
	fscanf(file,"%s",str);
	parse_check("rad:",str);
	fscanf(file,"%lf",r);
	//printf("rad: %f\n",*r);
}


void parse_shi(FILE*file,double *shi)
{
	char s[100];
	fscanf(file,"%s",s);
	parse_check("shi:",s);
	fscanf(file,"%lf",shi);
	//printf("shi: %f\n",*shi);
}


int loadScene(char *argv)
{
	FILE *file = fopen(argv,"r");
	int number_of_objects;
	char type[50];
	int i;
	Triangle t;
	Sphere s;
	Light l;
	fscanf(file,"%i",&number_of_objects);

	//printf("number of objects: %i\n",number_of_objects);

	double temp[3];
	parse_doubles(file,"amb:",temp);
	ambient_light.r = temp[0];
	ambient_light.g = temp[1];
	ambient_light.b = temp[2];

	for(i=0;i < number_of_objects;i++) {
		fscanf(file,"%s\n",type);
		//printf("%s\n",type);
		if(stricmp(type,"triangle")==0) {

			//printf("found triangle\n");
			int j;

			for(j=0;j < 3;j++) {
				parse_doubles(file,"pos:",temp);
				t.v[j].position.x = temp[0];
				t.v[j].position.y = temp[1];
				t.v[j].position.z = temp[2];
				parse_doubles(file,"nor:",temp);
				t.v[j].normal.x = temp[0];
				t.v[j].normal.y = temp[1];
				t.v[j].normal.z = temp[2];
				parse_doubles(file,"dif:",temp);
				t.v[j].color_diffuse.r = temp[0];
				t.v[j].color_diffuse.g = temp[1];
				t.v[j].color_diffuse.b = temp[2];
				parse_doubles(file,"spe:",temp);
				t.v[j].color_specular.r = temp[0];
				t.v[j].color_specular.g = temp[1];
				t.v[j].color_specular.b = temp[2];
				parse_shi(file,&t.v[j].shininess);
			}

			if(num_triangles == MAX_TRIANGLES) {
				printf("too many triangles, you should increase MAX_TRIANGLES!\n");
				return 0;
			}
			triangles[num_triangles++] = t;
		}
		else if(stricmp(type,"sphere")==0) {
			//printf("found sphere\n");

			parse_doubles(file,"pos:",temp);
			s.position.x = temp[0];
			s.position.y = temp[1];
			s.position.z = temp[2];
			parse_rad(file,&s.radius);
			parse_doubles(file,"dif:",temp);
			s.color_diffuse.r = temp[0];
			s.color_diffuse.g = temp[1];
			s.color_diffuse.b = temp[2];
			parse_doubles(file,"spe:",temp);
			s.color_specular.r = temp[0];
			s.color_specular.g = temp[1];
			s.color_specular.b = temp[2];
			parse_shi(file,&s.shininess);

			if(num_spheres == MAX_SPHERES) {
				printf("too many spheres, you should increase MAX_SPHERES!\n");
				return 0;
			}
			spheres[num_spheres++] = s;
		}
		else if(stricmp(type,"light")==0) {
			//printf("found light\n");
			parse_doubles(file,"pos:",temp);
			l.position.x = temp[0];
			l.position.y = temp[1];
			l.position.z = temp[2];
			parse_doubles(file,"col:",temp);
			l.color.r = temp[0];
			l.color.g = temp[1];
			l.color.b = temp[2];

			if(num_lights == MAX_LIGHTS) {
				printf("too many lights, you should increase MAX_LIGHTS!\n");
				return 0;
			}
			lights[num_lights++] = l;
		}
		else {
			printf("unknown type in scene description:\n%s\n",type);
			return 0;
		}
	}
	return 0;
}


void display()
{

}


void init()
{
	glMatrixMode(GL_PROJECTION);
	glOrtho(0,WIDTH,0,HEIGHT,1,-1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT);
}


void idle()
{
	unsigned int x,y;

	draw_scene();
	
	if (!animation) {
		
		if (count > 0) {
			return;
		}
		
		for(y = 0; y < HEIGHT; y++) {

			glPointSize(2.0);
			glBegin(GL_POINTS);

			for(x=0; x < WIDTH; x++) {
				plot_pixel(x, y, superRay[(HEIGHT-1-y)*WIDTH + x].color.r * 255, superRay[(HEIGHT-1-y)*WIDTH + x].color.g * 255, superRay[(HEIGHT-1-y)*WIDTH + x].color.b * 255);
				
			}

			glEnd();
			glFlush();
		}
		
		if (mode == MODE_JPEG) {
			save_jpg(count);
		}
		
		count ++;
		return;
	}
	
	
	if (motionBlur) {
		
		if (count!=0 && count%2 == 0) {

			for(y = 0; y < HEIGHT; y++) {
				for(x=0; x < WIDTH; x++) {
					motionColor[(HEIGHT-1-y)*WIDTH + x][0] = motionColor[(HEIGHT-1-y)*WIDTH + x][2];
				}
			}
		}


		for(y = 0; y < HEIGHT; y++) {

			for(x=0; x < WIDTH; x++) {
				
				motionColor[(HEIGHT-1-y)*WIDTH + x][2 - count%2].r = superRay[(HEIGHT-1-y)*WIDTH + x].color.r * 255;
				motionColor[(HEIGHT-1-y)*WIDTH + x][2 - count%2].g = superRay[(HEIGHT-1-y)*WIDTH + x].color.g * 255;
				motionColor[(HEIGHT-1-y)*WIDTH + x][2 - count%2].b = superRay[(HEIGHT-1-y)*WIDTH + x].color.b * 255;
			}
		}

		if (count!=0 && count%2 == 0) {

			for(y = 0; y < HEIGHT; y++) {
				glPointSize(2.0);
				glBegin(GL_POINTS);

				for(x=0; x < WIDTH; x++) {
					plot_pixel(x, y, (0.25 * motionColor[(HEIGHT-1-y)*WIDTH + x][0].r + 0.5 * motionColor[(HEIGHT-1-y)*WIDTH + x][1].r + 0.25 * motionColor[(HEIGHT-1-y)*WIDTH + x][2].r),
									 (0.25 * motionColor[(HEIGHT-1-y)*WIDTH + x][0].g + 0.5 * motionColor[(HEIGHT-1-y)*WIDTH + x][1].g + 0.25 * motionColor[(HEIGHT-1-y)*WIDTH + x][2].g),
									 (0.25 * motionColor[(HEIGHT-1-y)*WIDTH + x][0].b + 0.5 * motionColor[(HEIGHT-1-y)*WIDTH + x][1].b + 0.25 * motionColor[(HEIGHT-1-y)*WIDTH + x][2].b));
				}
				glEnd();
				glFlush();
			}

			if (mode == MODE_JPEG) {
				save_jpg(count/2);
			}
		}

		if (num_spheres > 0) {
			spheres[0].position.x -= 0.02;
		}
		if (num_triangles > 0) {
			triangles[0].v[0].position.x += 0.02;
			triangles[0].v[1].position.x += 0.02;
			triangles[0].v[2].position.x += 0.02;
		}
		
	} else {
	
		for(y = 0; y < HEIGHT; y++) {

			glPointSize(2.0);
			glBegin(GL_POINTS);

			for(x=0; x < WIDTH; x++) {
				plot_pixel(x, y, superRay[(HEIGHT-1-y)*WIDTH + x].color.r * 255, superRay[(HEIGHT-1-y)*WIDTH + x].color.g * 255, superRay[(HEIGHT-1-y)*WIDTH + x].color.b * 255);
				
			}

			glEnd();
			glFlush();
		}

		if (num_spheres > 0) {
			spheres[0].position.x -= 0.04;
		}
		if (num_triangles > 0) {
			triangles[0].v[0].position.x += 0.04;
			triangles[0].v[1].position.x += 0.04;
			triangles[0].v[2].position.x += 0.04;
		}
		
		if (mode == MODE_JPEG) {
			save_jpg(count);
		}
	
	}

	count ++;
}


void keyboard(unsigned char key, int x, int y)
{
	int w, h;
	switch (key) {
	case 'a':
		animation = !animation;
		count = 0;
		break;
	case 'm':
		motionBlur = !motionBlur;
		break;
	case 's':
		count = 0;
		softShadows = !softShadows;
		if (softShadows) {
			num_lights = newLights;
			for(int i = 0 ; i < initialLights; i++) {
				lights[i].color.r = lights[i].color.r / 28;
				lights[i].color.g = lights[i].color.g / 28;
				lights[i].color.b = lights[i].color.b / 28;
			}
		} else {
			num_lights = initialLights;
			for(int i = 0 ; i < initialLights; i++) {
				lights[i].color.r = lights[i].color.r * 28;
				lights[i].color.g = lights[i].color.g * 28;
				lights[i].color.b = lights[i].color.b * 28;
			}
		}
		break;
	}
	idle();
}


int main (int argc, char ** argv)
{
	if (argc<2 || argc > 3) {
		printf ("usage: %s <scenefile> [jpegname]\n", argv[0]);
		return 0;
	}
	if(argc == 3) {
		mode = MODE_JPEG;
		filename = argv[2];
	}
	else if(argc == 2)
		mode = MODE_DISPLAY;

	glutInit(&argc,argv);

	loadScene(argv[1]);

	
	//-------------------- Convert to Area Light ------------------------//
	initialLights = num_lights;
	int numAdded = 0;
	double delta = 0.05;

	for(int i = 0 ; i < initialLights; i++) {

		lights[i].color.r = lights[i].color.r;
		lights[i].color.g = lights[i].color.g;
		lights[i].color.b = lights[i].color.b;

		for (int j = 1; j >= -1; j--) {

			for (int k = -1; k <= 1; k++) {

				for (int l = -1; l <=1; l++) {

					lights[initialLights + numAdded].color.r = lights[i].color.r / 28 ;
					lights[initialLights + numAdded].color.g = lights[i].color.r / 28 ;
					lights[initialLights + numAdded].color.b = lights[i].color.r / 28 ;

					lights[initialLights + numAdded].position.x = lights[i].position.x + k * delta;
					lights[initialLights + numAdded].position.y = lights[i].position.y + l * delta;
					lights[initialLights + numAdded].position.z = lights[i].position.z + j * delta;

					numAdded++;

				}
			}
		}
	}
	newLights = num_lights + (27*num_lights);
	//-------------------------------------------------------------------//
	


	//printElements();
	initializeRays();	
	
	glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE);
	glutInitWindowPosition(0,0);
	glutInitWindowSize(WIDTH,HEIGHT);
	int window = glutCreateWindow("Ray Tracer");
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutIdleFunc(idle);
	init();
	glutMainLoop();
	

	getchar();
	return 0;
}